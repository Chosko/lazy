﻿using UnityEngine;
using System.Collections;

public class TriggerRed : MonoBehaviour {
    public GameObject target;
    public bool repeat = false;
    private bool done = false;

    void OnTriggerEnter(Collider collider)
    {
        if (done && !repeat) return;
        if (collider.tag == "Player")
        {
            done = true;
            Debug.Log("turn red triggered");
            ((TurningRed)target.GetComponent<TurningRed>()).TurnRed();
        }
    }
}
