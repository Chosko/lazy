﻿using UnityEngine;
using System.Collections;

public class EnableDisableTrigger : MonoBehaviour {
    public enum TriggerAction
    {
        ENABLE,
        DISABLE
    }

    public GameObject target;
    public TriggerAction action = TriggerAction.ENABLE;
    public bool repeat = false;
    public bool resetPlayer = false;
    public GameObject player;
    private bool done = false;
    public float wait = 0f;

    public void OnTriggerEnter(Collider collider){
        if (done && !repeat) return;
        if (collider.tag == "Player")
        {
            done = true;
            if (wait > 0)
            {
                StartCoroutine(Wait());
            }
            else
            {
                ApplyAction();
            }
            
        }
    }

    private void ApplyAction()
    {
        if (action == TriggerAction.ENABLE)
            target.SetActive(true);
        else
            target.SetActive(false);
        if (resetPlayer)
        {
            player.transform.position = new Vector3(0, 1.53f, 0);
            player.transform.eulerAngles = new Vector3(0, 0, 0);
        }
    }

    // Update is called once per frame
    private IEnumerator Wait()
    {
        yield return new WaitForSeconds(wait);
        ApplyAction();
    }
}
