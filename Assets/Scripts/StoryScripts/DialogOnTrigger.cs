﻿using UnityEngine;
using System.Collections;

public class DialogOnTrigger : MonoBehaviour {

    public AudioClip dialog;
    public float wait = 0;
    public bool repeat = false;
    private bool done = false;

    public void OnTriggerEnter(Collider collider)
    {
        if (done && !repeat) return;
        if (collider.tag == "Player")
        {
            done = true;
            if (wait > 0)
            {
                StartCoroutine(Wait());
            }
            else
            {
                DialogueManager.Instance.BeginDialog(dialog);
            }
        }
    }

    private IEnumerator Wait()
    {
        yield return new WaitForSeconds(wait);
        Debug.Log("launch trigger");
        DialogueManager.Instance.BeginDialog(dialog);

    }
}
