﻿using UnityEngine;
using System.Collections;

public class WaitForActivate : MonoBehaviour
{
    public enum TriggerAction
    {
        ENABLE,
        DISABLE
    }

    public GameObject target;
    public TriggerAction action = TriggerAction.ENABLE;
    public float wait = 2f;

    void OnEnable()
    {
        StartCoroutine(Wait());
    }

    // Update is called once per frame
    private IEnumerator Wait()
    {
        yield return new WaitForSeconds(wait);
        if (action == TriggerAction.ENABLE)
            target.SetActive(true);
        else
            target.SetActive(false);
    }
}