﻿using UnityEngine;
using System.Collections;

public class OpenDoorOnTrigger : MonoBehaviour {

    public GameObject door;
    public bool repeat = false;
    private bool done = false;

    void OnTriggerEnter(Collider collider)
    {
        if (done && !repeat) return;
        if(collider.tag == "Player"){
            done = true;
            ((DoorAnimation)door.GetComponent<DoorAnimation>()).DoorOpen();
        }
    }
}
