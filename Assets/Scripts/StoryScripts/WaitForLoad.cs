﻿using UnityEngine;
using System.Collections;

public class WaitForLoad : MonoBehaviour
{

    public float wait = 2f;
    public string level;

    // Use this for initialization
    void Start()
    {
        Debug.Log("Start");
    }

    void OnEnable()
    {
        Debug.Log("Enable");
        StartCoroutine(Wait());
    }

    private IEnumerator Wait()
    {
        yield return new WaitForSeconds(wait);
        Debug.Log("lloading level");
		Application.LoadLevel(level);
    }
}
