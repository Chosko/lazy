﻿using UnityEngine;
using System.Collections;

public class TurningRed : MonoBehaviour {

    //public GameObject target;
	private Material original;
	public Material material2;
	public float multiplier = 0.3F;
	private float lerp;

	void Start ()
	{
		original = renderer.material;
	}

    private enum RedState
    {
        ENABLING,
        ENABLED,
        DISABLING,
        DISABLED
    }

    private RedState state = RedState.DISABLED;

    public void TurnRed()
    {
        state = RedState.ENABLING;
		lerp = 0;
    }

    public void TurnNormal()
    {
        state = RedState.DISABLING;
		lerp = 0;
    }
    
    // Update is called once per frame
	void Update () {
        //MeshRenderer mr = target.GetComponent<MeshRenderer>();
        switch (state)
        {
            case RedState.ENABLING:
                // turn smoothly red frame by frame
                // when completely red, set state = RedState.ENABLE
				lerp += Time.deltaTime*multiplier;
				if (lerp > 1)
				{
					state = RedState.ENABLED;
					lerp = 1;
				}
				renderer.material.Lerp(original, material2, lerp);
                break;
            case RedState.DISABLING:
                // turn smootlhy back to the original color
                // when completely reverted, set state = RedState.DISABLING
				lerp += Time.deltaTime*multiplier;
				if (lerp > 1)
				{
					state = RedState.DISABLED;
					lerp = 1;
				}
				renderer.material.Lerp(material2, original, lerp);
                break;
            default:
                break;
        }
	}
}
