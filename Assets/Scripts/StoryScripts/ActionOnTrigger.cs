﻿using UnityEngine;
using System.Collections;

public class ActionOnTrigger : MonoBehaviour {

    public AudioClip dialog;
    public float wait = 0;
    public bool repeat = false;
    private bool done = false;

	public GameObject panel;
	public GameObject mirino;

    public void OnTriggerEnter(Collider collider)
    {
        if (done && !repeat) return;
        if (collider.tag == "Player")
        {
            done = true;
            if (wait > 0)
            {
                StartCoroutine(Wait());
            }
            else
            {
				panel.SetActive(true);
				mirino.SetActive(false);
                DialogueManager.Instance.BeginDialog(dialog);
            }
        }
    }

    private IEnumerator Wait()
    {
        yield return new WaitForSeconds(wait);
        Debug.Log("launch trigger");
        DialogueManager.Instance.BeginDialog(dialog);
		panel.SetActive(true);
    }
}
