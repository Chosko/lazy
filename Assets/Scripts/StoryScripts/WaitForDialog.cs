﻿using UnityEngine;
using System.Collections;

public class WaitForDialog : MonoBehaviour
{

    public float wait = 2f;
    public AudioClip dialog;

    // Use this for initialization
    void Start()
    {
        Debug.Log("Start");
    }

    void OnEnable()
    {
        Debug.Log("Enable");
        StartCoroutine(Wait());
    }

    private IEnumerator Wait()
    {
        yield return new WaitForSeconds(wait);
        Debug.Log("launch dialog");
        DialogueManager.Instance.BeginDialog(dialog); 
    }
}
