﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameManager : Singleton<GameManager> {
	protected GameManager(){}

	public GameObject HUD;
    public GameObject Scene1;

	public void startHUD(){
		HUD.SetActive(true);
	}

	//usage GameUtils.instance.ActivateChildren();
	public void ActivateChildren(GameObject g, bool a){
		throw new System.NotImplementedException ();
	}
	
	public void DeactivateChildren(GameObject g, bool a) {
		//g.activeSelf = a;
		
		foreach (Transform child in g.transform) {
			DeactivateChildren(child.gameObject, a);
		}
	}
}
