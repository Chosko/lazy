﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerManager2D : MonoBehaviour {

	public Animator animator;
	public Vector2 velocity;
	public float maxVel;

	public GameObject doorDx;
	public GameObject doorSx;
	public Animator doorDxAnim;
	public Animator doorSxAnim;

	public Font pixel;
	public GameObject sub;

	public Sprite doorDxRed;

	public bool firstMove = false;
	public bool firstMoveRight = false;

	public AudioClip[] clips;

	public GameObject oldSubObj;
	public GameObject oldSubText;
	public GameObject pixelSubObj;
	public GameObject pixelSubText;

	// Use this for initialization
	void Start () {
		animator = gameObject.GetComponent<Animator> ();
		doorDxAnim = doorDx.GetComponent<Animator> ();
		doorSxAnim = doorSx.GetComponent<Animator>();

//		oldSubObj = DialogueManager.Instance.subtitleObj;
//		oldSubText = DialogueManager.Instance.subtitleObjTxt;
//
//		DialogueManager.Instance.subtitleObj = pixelSubObj;
//		DialogueManager.Instance.subtitleObjTxt = pixelSubText;

		DialogueManager.Instance.displaySub(".");
		sub.GetComponent<Text>().font = pixel;
		DialogueManager.Instance.BeginDialog(clips[0]);
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey(KeyCode.LeftArrow)){
			float newVel = gameObject.rigidbody2D.velocity.x + velocity.x;
			if (newVel >= maxVel)
				gameObject.rigidbody2D.velocity = new Vector2(maxVel,0f);
			else
				gameObject.rigidbody2D.velocity = new Vector2(newVel,0f);
			animator.SetTrigger("Walk");
		}
		else if (Input.GetKey(KeyCode.RightArrow)){
			float newVel = gameObject.rigidbody2D.velocity.x - velocity.x;
			if (newVel <= -maxVel)
				gameObject.rigidbody2D.velocity = new Vector2(-maxVel,0);
			else
				gameObject.rigidbody2D.velocity = new Vector2(newVel,0);
			animator.SetTrigger("Walk");	
		}
		else {
			float newVel = gameObject.rigidbody2D.velocity.x - velocity.x;
			if (newVel < 0)
				gameObject.rigidbody2D.velocity = Vector2.zero;
			else 
				gameObject.rigidbody2D.velocity = new Vector2(newVel,0f);
		}
		/*else {
			gameObject.rigidbody2D.velocity = new Vector2(0.0f,0.0f);

		}*/
	}

	void OnTriggerEnter2D(Collider2D coll){
		if (coll.name == "porta_dx"){
			Debug.Log("open dx door");
			doorDxAnim.SetTrigger("OpenDoor");
			float t = Time.time;
			while (Time.time - t > 5f){
			}
			Application.LoadLevel("Black_1");

		}
		else if (coll.name == "porta_sx") {
			doorSxAnim.SetTrigger("DoorClosed");
		}
		else if (coll.name == "TriggerAudioBad"){
			DialogueManager.Instance.BeginDialog(clips[2]);	
			highlightDoorDx();
			Destroy(coll);
		}
		else if (coll.name == "TriggerAudioGood"){
			DialogueManager.Instance.BeginDialog(clips[1]);
			Destroy(coll);
		}
	}

	void highlightDoorDx ()
	{
		//doorDxAnim.SetTrigger("Highlight");
		doorDx.GetComponent<SpriteRenderer>().color = new Color(255f,0f,0f);
	}
}
