﻿using UnityEngine;
using System.Collections;

enum MainMenuState {
	NORMAL_STATE,
	QUIT_STATE,
	VOICE_PLAYING};

public class MainMenuManager : MonoBehaviour {

	MainMenuState state = MainMenuState.NORMAL_STATE;

	// time between the "all-start-button" and the voice
	public int timeBeforeStart = 2;

	// narrator voice in menu events
	public AudioClip voice;

	// time init
	public float startTime;

	// interactions
	public int interactions = 0;

	// 
	public GameObject firstQuit;
	public GameObject secondStart;
	public GameObject secondQuit;
	public GameObject thirdStart;
	public GameObject thirdQuit;

	// Use this for initialization
	void Start () {
		startTime = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
		if (Time.time-startTime > timeBeforeStart && state == MainMenuState.QUIT_STATE) {
			DialogueManager.Instance.BeginDialog(audio.clip);
			state = MainMenuState.VOICE_PLAYING;
			startTime = Time.time;
		}
		else if (gameObject.activeSelf && state == MainMenuState.VOICE_PLAYING && Time.time-startTime > timeBeforeStart) {
			this.gameObject.SetActive(false);
            GameManager.Instance.Scene1.SetActive(true);
			GameManager.Instance.startHUD();
		}

	}

	public void addInteractions(){
		interactions++;
		if (interactions >= 3){
			state = MainMenuState.QUIT_STATE;
		}
	}

	public void startClick(){
		firstQuit.SetActive(true);
		secondStart.SetActive(true);
		addInteractions();
	}

	public void optionsClick(){
		secondQuit.SetActive(true);
		addInteractions();
	}

	public void creditsClick(){
		thirdQuit.SetActive(true);
		addInteractions();
	}

	public void secondStartClick(){
		secondQuit.SetActive(true);
		thirdStart.SetActive(true);
		addInteractions();
	}

	public void thirdStartClick(){
		thirdQuit.SetActive(true);
		addInteractions();
	}
}
