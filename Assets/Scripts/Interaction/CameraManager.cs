﻿using UnityEngine;
using System.Collections;

public class CameraManager : MonoBehaviour {

	private Vector3 rayHitWorldPosition;
	private RaycastHit colliderObj;
	private bool grabbed = false;
	private GameObject grabbedObj = null;

	public GameObject grabber;
	public float offset;
	public float distance;
	public float distanceShot;
	public int intensityForceShot;
	public AudioClip abc;

	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {

		if (grabbed) {
			grabbedObj.transform.position = grabber.transform.position;
			grabbedObj.transform.rotation = grabber.transform.rotation;
		}

		if (Input.GetKeyDown (KeyCode.Mouse0) && !grabbed) {
			Debug.Log ("I don't have an object");
			Vector3 fwd = transform.TransformDirection (Vector3.forward);

			if (Physics.Raycast (transform.position, fwd, out colliderObj, distance)) {
		
				GameObject objectHit = colliderObj.collider.gameObject;

				if (objectHit.tag == "Draggable") {
					Debug.Log ("change target dot HUD"); //TODO change target HUD
					grabbed = true;
					grabbedObj = objectHit;
					grabbedObj.rigidbody.freezeRotation = true;
					grabbedObj.rigidbody.useGravity = false;
				} else if (objectHit.tag == "Door") {
					Debug.Log ("change target dot HUD"); //TODO change target HUD
					objectHit.gameObject.SendMessage ("DoorAction"); 
				}
			}
		} else if (Input.GetKeyDown (KeyCode.Mouse0) && grabbed && grabbedObj.gameObject.name == "Gun") {
			Debug.Log("I have a gun");
			Vector3 fwd = transform.TransformDirection (Vector3.forward);
			if (Physics.Raycast (transform.position, fwd, out colliderObj, distanceShot)) {
				
				GameObject objectHit = colliderObj.collider.gameObject;
				
				if (objectHit.tag == "Killable") {
					Debug.Log("I killed something");
					//grabbedObj.rigidbody.AddForceAtPosition(Camera.main.transform.forward, objectHit.collider.transform.position);
					//objectHit.collider.gameObject.GetComponent<Animator>().SetTrigger(1);
					objectHit.collider.gameObject.rigidbody.AddForce(new Vector3(2000,2000,0));
					DialogueManager.Instance.BeginDialog(abc);
					//objectHit.collider.gameObject.animation.Play ("Alieno_Animation_02");
				}	
			}
		} else if (Input.GetKeyDown (KeyCode.Mouse0) && grabbed) {
			Vector3 fwd = transform.TransformDirection (Vector3.forward);
			if(Physics.Raycast (transform.position, fwd, out colliderObj, distance)) {
				GameObject objectHit = colliderObj.collider.gameObject;
				
				if (objectHit.tag == "Vendor" && grabbedObj.gameObject.name.Contains("Coin")) {
					Debug.Log("Interacting Vendor Machine");
					GameObject.Destroy(grabbedObj);

					//suono moneta inserita e ricaduta

					objectHit.collider.gameObject.SendMessage("Decrement");
					grabbed = false;
				}	
			}
			else if(!grabbedObj.gameObject.name.Contains("Coins")) {
				Debug.Log("I leave an object I had");
				grabbed = false;
				grabbedObj.rigidbody.freezeRotation = false;
				grabbedObj.rigidbody.useGravity = true;
			}
		}
	}
}
