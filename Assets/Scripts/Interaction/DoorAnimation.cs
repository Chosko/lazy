﻿using UnityEngine;
using System.Collections;

public class DoorAnimation : MonoBehaviour
{
    public enum DoorState
    {
        CLOSED,
        OPEN,
        CLOSING,
        OPENING
    }

    public enum DoorOpeningDirection
    {
        CLOCKWISE,
        ANTICLOCKWISE
    }

    public const float MULTIPLIER = 100f;

    public DoorOpeningDirection direction = DoorOpeningDirection.ANTICLOCKWISE;
    private DoorState state = DoorState.CLOSED;
    private AudioSource[] openSounds;
    private AudioSource[] closeSounds;
    private AudioSource[] squeekOpenSounds;

    private GameObject pivot;

    public void Awake()
    {
        pivot = gameObject.transform.parent.gameObject;
        AudioSource[] sources = gameObject.GetComponents<AudioSource>();
        openSounds = new AudioSource[2];
        openSounds[0] = sources[0];
        openSounds[1] = sources[1];
        squeekOpenSounds = new AudioSource[2];
        squeekOpenSounds[0] = sources[2];
        squeekOpenSounds[1] = sources[3];
        closeSounds = new AudioSource[1];
        closeSounds[0] = sources[4];
    }

    public void DoorAction()
    {
        switch (state)
        {
            case DoorState.CLOSED:
                DoorOpen();
                break;
            case DoorState.OPEN:
                DoorClose();
                break;
            case DoorState.CLOSING:
                break;
            case DoorState.OPENING:
                break;
        }
    }

    public void DoorClose()
    {
        if (state == DoorState.OPEN){
            state = DoorState.CLOSING;
            ChooseRand(closeSounds).Play();
        }
    }

    public void DoorOpen()
    {
        if (state == DoorState.CLOSED)
        {
            state = DoorState.OPENING;
            ChooseRand(openSounds).Play();
            ChooseRand(squeekOpenSounds).PlayDelayed(0.02f);
        }
    }

    private AudioSource ChooseRand(AudioSource[] asrc)
    {
        return asrc[Random.Range(0, asrc.Length - 1)];
    }

    void FixedUpdate()
    {
        switch (state)
        {
            case DoorState.CLOSED:
                break;
            case DoorState.OPEN:
                break;
            case DoorState.CLOSING:
                if (direction == DoorOpeningDirection.ANTICLOCKWISE)
                {
                    pivot.transform.Rotate(new Vector3(0, -MULTIPLIER * Time.deltaTime, 0));
                    if (pivot.transform.localEulerAngles.y < 90)
                    {
                        pivot.transform.localEulerAngles = new Vector3(0, 90, 0);
                        state = DoorState.CLOSED;
                    }
                }
                else
                {
                    pivot.transform.Rotate(new Vector3(0, MULTIPLIER * Time.deltaTime, 0));
                    if (pivot.transform.localEulerAngles.y > 90)
                    {
                        pivot.transform.localEulerAngles = new Vector3(0, 90, 0);
                        state = DoorState.CLOSED;
                    }
                }
                break;
            case DoorState.OPENING:
                if (direction == DoorOpeningDirection.ANTICLOCKWISE)
                {
                    pivot.transform.Rotate(new Vector3(0, MULTIPLIER * Time.deltaTime, 0));
                    if (pivot.transform.localEulerAngles.y > 180)
                    {
                        pivot.transform.localEulerAngles = new Vector3(0, 180, 0);
                        state = DoorState.OPEN;
                    }
                }
                else
                {
                    pivot.transform.Rotate(new Vector3(0, -MULTIPLIER * Time.deltaTime, 0));
                    if (pivot.transform.localEulerAngles.y <= 0 || pivot.transform.localEulerAngles.y > 180)
                    {
                        pivot.transform.localEulerAngles = new Vector3(0, 0, 0);
                        state = DoorState.OPEN;
                    }
                }
                break;
        }
    }
}