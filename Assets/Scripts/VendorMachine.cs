﻿using UnityEngine;
using System.Collections;

public class VendorMachine : MonoBehaviour {

	public int iteration;
	public Transform positionCoinSpawn;
	public GameObject vendor;
	public GameObject coin;
	public AudioClip dialog;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void Decrement() {
		iteration--;
		audio.Play ();

		Debug.Log ("insaztiamo la moneta");
		coin = (GameObject)GameObject.Instantiate (coin);
		coin.transform.position = positionCoinSpawn.position;
		if (iteration == 0) {
			vendor.SetActive (false);

			DialogueManager.Instance.BeginDialog(dialog);

			coin.SetActive(false);
		}	
	}
}
