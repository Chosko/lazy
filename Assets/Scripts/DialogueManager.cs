﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class DialogueManager : MonoBehaviour {

	public static DialogueManager Instance {get; private set;}

	// Audio Variables
	private AudioClip dialogueAudio;
	private const float _RATE = 44100.0f;

	// Dialogue Variables
	private string[] fileLines;
	private List<string> subtitleLines = new List<string>();
//	private List<string> subtitleTimingStrings = new List<string>();
	private List<float> subtitleTimings = new List<float>();
	private List<string> subtitleText = new List<string>();
	private int nextSubtitle = 0;
	private string displaySubtitle = null;


	//subtitle variables
	public GameObject subtitleObj;
	public GameObject subtitleObjTxt;
	public Text subtitleTextComponent;
	public static string currentString = null;

	private float lastUpdated = 0;

	void Awake () {
		if (Instance != null && Instance != this){
			Destroy(gameObject);
			return;
		}

		Instance = this;
		gameObject.AddComponent<AudioSource>();
		subtitleTextComponent = subtitleObjTxt.GetComponent<Text>();
	}

	void Update() {

		// update string text
		if (displaySubtitle != currentString) {
			displaySub(displaySubtitle);
			currentString = displaySubtitle;
			lastUpdated = Time.time;
		}

		// load the next subtitle
		if (nextSubtitle < subtitleText.Count) {
			if (audio.timeSamples / _RATE > subtitleTimings[nextSubtitle]) {
				displaySubtitle = subtitleText[nextSubtitle];
				nextSubtitle++;
			}
		}

		// after 5 seconds
		if (Time.time - lastUpdated > 3){
			subtitleObj.SetActive(false);
			lastUpdated = 0;
		}
	}

	public void BeginDialog(AudioClip clip) {
		subtitleLines = new List<string>();
		subtitleTimings = new List<float>();
		subtitleText = new List<string>();
		nextSubtitle = 0;
		currentString = null;

		//resetVariables();

		dialogueAudio = clip;

		TextAsset tAsset = Resources.Load("Dialogues/" + dialogueAudio.name) as TextAsset;
		fileLines = tAsset.text.Split('\n');

		foreach (string line in fileLines){
			if (line != "")
				subtitleLines.Add (line);
		}

		for (int cnt=0;cnt < subtitleLines.Count;cnt++){
			string[] splitTmp = subtitleLines[cnt].Split('|');
			subtitleTimings.Add (float.Parse(splitTmp[0]));
			subtitleText.Add(splitTmp[1]);
		}

		if (subtitleText[0] != null) {
			displaySubtitle = subtitleText[0];
		}


		audio.clip = clip;
		audio.Play ();
	}

	private void resetVariables() {

	}

	public void displaySub(string text){
		subtitleObj.SetActive(true);
		subtitleTextComponent.text = text;
	}

	public void addTestSubtitleUno(){
		displaySub("zero uno due tre quattro cinque sei sette otto nove dieci");
	}

	public void addTestSubtitleDue(){
		displaySub("dieci nove otto sette sei cinque quattro tre due uno zero");
	}
	
}
