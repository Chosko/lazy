﻿using UnityEngine;
using System.Collections;

public class TestDialogue : MonoBehaviour {

	public AudioClip testDialogue;

	public void startTestDialogue(){
		DialogueManager.Instance.BeginDialog(testDialogue);
	}
}
