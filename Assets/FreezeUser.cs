﻿using UnityEngine;
using System.Collections;

public class FreezeUser : MonoBehaviour {

    public GameObject player;
    public GameObject playerCamera;
    public GameObject dummyCamera;
    public GameObject aim;
    public float duration = 0;

    void OnEnable()
    {
        Transform t = playerCamera.transform;
        dummyCamera.transform.eulerAngles = t.eulerAngles;
        dummyCamera.transform.position = t.position;
        playerCamera.camera.enabled = false;
        player.SetActive(false);
        aim.SetActive(false);
        dummyCamera.SetActive(true);
        dummyCamera.camera.enabled = true;

        StartCoroutine(Wait());
    }

    private IEnumerator Wait()
    {
        yield return new WaitForSeconds(duration);
        player.SetActive(true);
        dummyCamera.camera.enabled = false;
        dummyCamera.SetActive(false);
        aim.SetActive(true);
        playerCamera.camera.enabled = true;
    }
}
